;;;; qsolve.asd
;;;;
;;;; Copyright (c) 2011-2015 Robert Smith

(asdf:defsystem #:qsolve
  :version "0.1"
  :author "Robert Smith"
  :description "Rubik-like puzzle utilities."
  :long-description "Scramblers and solvers for Rubik-like puzzles."
  :serial t
  :components ((:file "package")
               (:file "utilities")
               (:file "kociemba")
               (:file "group")
               (:file "3x3")
               (:file "god")
               
               (:file "tests"))
  :depends-on (#:cl-algebraic-data-type
               #:cl-permutation
               #:split-sequence
               #:alexandria
               #:global-vars))
