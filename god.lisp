;;;; god.lisp
;;;; Copyright (c) 2013 Robert Smith

(in-package #:qsolve)

;;;; A basic God's Algorithm
;;;;
;;;; Example:
;;;;
;;;; (let* ((perm (make-3x3-cube))
;;;;        (arr (perm::perm.rep perm))
;;;;        (spec (array-to-permutation-spec arr)))
;;;;   (god spec arr '(R (R -1) (R 2) (U 2))))

(defun god (spec solved-puzzle generator)
  (let ((distance (make-hash-table))
        (depth -1)
        (positions-left 1)
        (current-stack nil)
        (next-stack nil)
        (total 1)
        (moves (mapcar #'interpret-move generator)))
    (flet ((apply-move (move pos)
             (perm:rank spec (perm:permute move (perm:unrank spec pos)))))
      (declare (dynamic-extent (function apply-move))
               (inline apply-move))
      
      ;; Solved position has a distance of zero.
      (let ((solved-rank (perm:rank spec solved-puzzle)))
        (push solved-rank current-stack)
        (setf (gethash solved-rank distance) 0))
      
      ;; Loop through all of the positions
      (loop :do
        (rotatef next-stack current-stack)
        (setf positions-left 0)
        (incf depth)
        (loop :while (not (null current-stack)) :do
          (let ((pos (pop current-stack)))
            (dolist (move moves)
              (let ((new-pos (apply-move move pos)))
                (unless (nth-value 1 (gethash new-pos distance))
                  (setf (gethash new-pos distance) depth)
                  (push new-pos next-stack)
                  (incf positions-left))))))
        (unless (zerop depth)
          (format t "~D position~:P at depth ~D~%" positions-left depth)
          (incf total positions-left))
        :until (null next-stack))
      
      ;; Return God's table
      (format t  "---------------------~%~D total~%" total)
      
      distance)))
