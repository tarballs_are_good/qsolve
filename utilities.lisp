;;;; utilities.lisp
;;;; Copyright (c) 2013 Robert Smith

(in-package #:qsolve)

(defmacro constant-load-time-value (value)
  `(load-time-value ,value t))

(declaim (inline get-time))
(defun get-time ()
  (get-internal-real-time))

(declaim (inline get-time-since))
(defun get-time-since (time)
  (round (* 1000 (- (get-internal-real-time) time))
         internal-time-units-per-second))

(defmacro defconstant-early (var val &optional doc)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (defconstant ,var ,val ,@(if doc (list doc) nil))))
