;;;; kociemba.lisp
;;;; Copyright (c) 2013 Robert Smith

(in-package #:qsolve)

(declaim (optimize (speed 0) safety debug))
;;(declaim (optimize speed (safety 0) (debug 0) (space 0)))

;; (dotimes (i 3) (time (execute-tests "./tests/tests-medium.sexp")))
;;
;; 37.xx             // coord structs
;; 35.40 35.41 35.41 // typed coord structs
;; 34.92 35.13 35.11 // (or null move) move list
;; 34.06 34.16 33.90 // phase-2 UD-move check
;; 35.x              // transition table struct
;; 33,42 33.45 33.55 // (SPACE 0)
;; 32.47 32.45 32.47 // shared transition in phase1/2
;; 25.48 25.48 25.40 // inline small functions
;; 21.47 21.43 21.42 // null LAST in phase1/2
;; 19.40 19.35 19.37 // typed phase1/2
;; 13.40 13.42 13.44 // transition tables as SIMPLE-ARRAY
;; 10.63 10.61 10.64 // prune table struct

;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Utilities ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *debug* nil)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun factorial (n)
    (case n
      ((0 1) 1)
      (t (loop :for i :from 2 :to n
               :for fac := i :then (* i fac)
               :finally (return fac)))))
  
  (defun choose (n k)
    (/ (factorial n)
       (* (factorial k)
          (factorial (- n k))))))

(defmacro defenum (type-name &body names)
  "Define a C-style enumeration, whose type is named TYPE-NAME and
whose enum constants are NAMES. Also define <TYPE-NAME>-LIST which
contains a list of all of the constants."
  `(progn
     (deftype ,type-name ()
       '(mod ,(length names)))
     
     ,@(loop :for i :from 0
             :for name :in names 
             :collect `(defconstant ,name ,i))

     (defparameter ,(intern (format nil "~A-LIST" type-name))
       ',(loop :for i :below (length names) :collect i))
     
     ',type-name))

(deftype non-negative-fixnum ()
  '(and fixnum unsigned-byte))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Move Types ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; N.B. The orderings of these enums do matter.

(defenum move-plane
  UD
  FB
  RL)

(defenum move
  ;; UD plane
  U1 U2 U3
  D1 D2 D3
  ;; FB plane
  F1 F2 F3
  B1 B2 B3
  ;; RL plane
  R1 R2 R3
  L1 L2 L3)

(defconstant-early +num-moves+ 18)

(defmacro do-quarter-moves (var &body body)
  "Iterate through each of the single-turn CW quarter moves (U1, D1, F1, ...), binding VAR to each move within BODY."
  `(loop :for ,var :from U1 :below +num-moves+ :by 3
         :do (tagbody
                ,@body)))

(defenum face
  up
  down
  front
  back
  right
  left)

(defun face-string (face)
  "String representation of the face FACE."
  (svref '#.(map 'simple-vector #'string "UDFBRL") face))

(declaim (inline move-face))
(defun move-face (move)
  "The face of the move MOVE."
  (declare (type move move))
  (values (floor move 3)))

(declaim (inline move-multiplicity))
(defun move-multiplicity (move)
  "The multiplicity of the move MOVE (1, 2, or 3)."
  (declare (type move move))
  (1+ (mod move 3)))

(declaim (inline move-plane))
(defun move-plane (move)
  "The plane of the move MOVE."
  (declare (type move move))
  (values (floor move 6)))

(declaim (inline decompose-move))
(defun decompose-move (move)
  "Convert MOVE to a face and multiplicity."
  (declare (type move move))
  #+equivalent-code
  (values
   (move-face move)
   (move-multiplicity move))
  (multiple-value-bind (face multiplicity) (floor move 3)
    (declare (type face face)
             (type (mod 3) multiplicity))
    (values
     face
     (1+ multiplicity))))

(defun move-string (move)
  (multiple-value-bind (face multiplicity) (decompose-move move)
    (string-right-trim '(#\Space)
                       (format nil
                               "~A~C"
                               (face-string face)
                               (char "  2'" multiplicity)))))
(declaim (inline is-ud-move-p))
(defun is-ud-move-p (move)
  (declare (type move move))
  (= UD (move-plane move)))

(declaim (inline is-fru-move-p))
(defun is-fru-move-p (move)
  (declare (type move move))
  (< (mod move 6) 3))

(declaim (inline same-face-p))
(defun same-face-p (m1 m2)
  (declare (type move m1 m2))
  (= (move-face m1) (move-face m2)))

(declaim (inline parallel-moves-p))
(defun parallel-moves-p (m1 m2)
  (declare (type move m1 m2))
  (= (move-plane m1) (move-plane m2)))

(declaim (inline construct-move))
(defun construct-move (face mult)
  (declare (type face face)
           (type (integer 1 3) mult))
  (+ (1- mult) (* face 3)))

(defun combine-moves (m1 m2)
  (multiple-value-bind (face mult) (decompose-move m1)
    (let ((new-mult (mod (+ mult (move-multiplicity m2)) 4)))
      (if (zerop new-mult)
          nil
          (construct-move face new-mult)))))

(defun simplify-moves (moves)
  (flet ((add-move (combined next)
           (cond
             ((null combined)
              (cons next combined))
             
             ((same-face-p next (car combined))
              (let ((new (combine-moves next (first combined))))
                (if (null new)
                    (rest combined)
                    (cons new (rest combined)))))
             
             ((parallel-moves-p next (first combined))
              (let ((second (second combined)))
                (if (and second (same-face-p next second))
                    (let ((new (combine-moves next second)))
                      (if (null new)
                          (cons (first combined) (rest (rest combined)))
                          (list* (first combined) new (rest (rest combined)))))
                    (cons next combined))))
             
             (t (cons next combined)))))
    (nreverse (reduce #'add-move moves :initial-value nil))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Cubies ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; Corners

(defenum corner
  UFR URB UBL ULF
  DRF DFL DLB DBR)

(defconstant-early +num-corners+ 8)

(deftype corner-orientation ()
  ;; 0 = not rotated
  ;; 1 = rotated CCW
  ;; 2 = rotated CW
  '(mod 3))

(declaim (type (simple-array corner-orientation (#.+num-corners+)) **co**))
(global-vars:define-global-var* **co**
    (make-array +num-corners+ :element-type 'corner-orientation
                              :initial-element 0))

(declaim (type (simple-array corner (#.+num-corners+)) **cp**))
(global-vars:define-global-var* **cp**
    (make-array +num-corners+ :element-type 'corner
                              :initial-element 0))

(defconstant-early +num-co+ (expt 3 (1- +num-corners+)))
(defconstant-early +num-cp+ (factorial +num-corners+))

;;;; Edges

(defenum edge
  UF  UR  UB  UL
  DF  DR  DB  DL
  FR  FL  BR  BL)

(defconstant-early +num-edges+ 12)            ; FIXME: Make 8?

(deftype edge-orientation ()
  ;; 0 = not flipped
  ;; 1 = flipped
  '(mod 2))

(defstruct cubie-cube
  eo
  ep
  co
  cp)

(declaim (type (simple-array edge-orientation (#.+num-edges+)) **eo**))
(global-vars:define-global-var* **eo**
    (make-array +num-edges+ :element-type 'edge-orientation
                            :initial-element 0))

(declaim (type (simple-array edge (#.+num-edges+)) **ep**))
(global-vars:define-global-var* **ep**
    (make-array +num-edges+ :element-type 'edge
                            :initial-element 0))

(defconstant-early +num-eo+ (expt 2 (1- +num-edges+)))
(defconstant-early +num-ep+ (factorial 8))

;;;; UD Slice

;;; The edges of the UD slice are represented by the last four edges
;;; of the edge representation. As such, an edge permutation only
;;; represents the first 8, non-E-slice edges.

;;; XXX: Use a displaced array to represent the UD slice?

(defconstant-early +num-ud1+ (choose 12 4))

(defconstant-early +num-ud2+ (factorial 4))

;;;;;;;;;;;;;;;;;;;;;;;;;;; Pruning Tables ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconstant +max-prune-distance+ 9999)

(defstruct phase-1-prune
  (eo (make-array #.+num-eo+ :element-type 'non-negative-fixnum
                             :initial-element +max-prune-distance+)
   :type (simple-array non-negative-fixnum (#.+num-eo+)))
  (co (make-array #.+num-co+ :element-type 'non-negative-fixnum
                             :initial-element +max-prune-distance+)
   :type (simple-array non-negative-fixnum (#.+num-co+)))
  (ud1 (make-array #.+num-ud1+ :element-type 'non-negative-fixnum
                               :initial-element +max-prune-distance+)
   :type (simple-array non-negative-fixnum (#.+num-ud1+))))

(declaim (type phase-1-prune **phase-1-prune**))
(global-vars:define-global-var* **phase-1-prune** (make-phase-1-prune))

(define-symbol-macro *eo-prune* (phase-1-prune-eo **phase-1-prune**))
(define-symbol-macro *co-prune* (phase-1-prune-co **phase-1-prune**))
(define-symbol-macro *ud1-prune* (phase-1-prune-ud1 **phase-1-prune**))

(defstruct phase-2-prune
  (ep (make-array #.+num-ep+ :element-type 'non-negative-fixnum
                             :initial-element +max-prune-distance+)
   :type (simple-array non-negative-fixnum (#.+num-ep+)))
  (cp (make-array #.+num-cp+ :element-type 'non-negative-fixnum
                             :initial-element +max-prune-distance+)
   :type (simple-array non-negative-fixnum (#.+num-cp+)))
  (ud2 (make-array #.+num-ud2+ :element-type 'non-negative-fixnum
                               :initial-element +max-prune-distance+)
   :type (simple-array non-negative-fixnum (#.+num-ud2+))))

(declaim (type phase-2-prune **phase-2-prune**))
(global-vars:define-global-var* **phase-2-prune** (make-phase-2-prune))

(define-symbol-macro *ep-prune* (phase-2-prune-ep **phase-2-prune**))
(define-symbol-macro *cp-prune* (phase-2-prune-cp **phase-2-prune**))
(define-symbol-macro *ud2-prune* (phase-2-prune-ud2 **phase-2-prune**))


;;;;;;;;;;;;;;;;;;;;;;;;; Transition Tables ;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct phase-1-transition
  (eo (make-array '(#.+num-eo+ 6) :element-type '(integer 0 (#.+num-eo+))
                                  :initial-element 0)
   :type (simple-array (integer 0 (#.+num-eo+)) (#.+num-eo+ 6)))
  (co (make-array '(#.+num-co+ 6) :element-type '(integer 0 (#.+num-co+))
                                  :initial-element 0)
   :type (simple-array (integer 0 (#.+num-co+)) (#.+num-co+ 6)))
  (ud1 (make-array '(#.+num-ud1+ 6) :element-type '(integer 0 (#.+num-ud1+))
                                    :initial-element 0)
   :type (simple-array (integer 0 (#.+num-ud1+)) (#.+num-ud1+ 6))))

(declaim (type phase-1-transition **phase-1-transition**))
(global-vars:define-global-var* **phase-1-transition** (make-phase-1-transition))

(define-symbol-macro *eo-trans* (phase-1-transition-eo **phase-1-transition**))
(define-symbol-macro *co-trans* (phase-1-transition-co **phase-1-transition**))
(define-symbol-macro *ud1-trans* (phase-1-transition-ud1 **phase-1-transition**))

(defstruct phase-2-transition
  (ep (make-array '(#.+num-ep+ 6) :element-type '(integer 0 (#.+num-ep+))
                                  :initial-element 0)
   :type (simple-array (integer 0 (#.+num-ep+)) (#.+num-ep+ 6)))
  (cp (make-array '(#.+num-cp+ 6) :element-type '(integer 0 (#.+num-cp+))
                                  :initial-element 0)
   :type (simple-array (integer 0 (#.+num-cp+)) (#.+num-cp+ 6)))
  (ud2 (make-array '(#.+num-ud2+ 6) :element-type '(integer 0 (#.+num-ud2+))
                                    :initial-element 0)
   :type (simple-array (integer 0 (#.+num-ud2+)) (#.+num-ud2+ 6))))

(declaim (type phase-2-transition **phase-2-transition**))
(global-vars:define-global-var* **phase-2-transition** (make-phase-2-transition))

(define-symbol-macro *ep-trans* (phase-2-transition-ep **phase-2-transition**))
(define-symbol-macro *cp-trans* (phase-2-transition-cp **phase-2-transition**))
(define-symbol-macro *ud2-trans* (phase-2-transition-ud2 **phase-2-transition**))

(global-vars:define-global-var* *tables-initialized* nil)


;;;;;;;;;;;;;;;;;;;;;;;;;;;; Coordinates ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; G0 Coordinates

(defstruct phase-1-coordinate
  (eo 0  :type (integer 0 (#.+num-eo+)))
  (co 0  :type (integer 0 (#.+num-co+)))
  (ud1 0 :type (integer 0 (#.+num-ud1+))))

(declaim (type phase-1-coordinate **phase-1-coordinate**))
(global-vars:define-global-var* **phase-1-coordinate** (make-phase-1-coordinate))

(define-symbol-macro *eo-coord* (phase-1-coordinate-eo **phase-1-coordinate**))
(define-symbol-macro *co-coord* (phase-1-coordinate-co **phase-1-coordinate**))
(define-symbol-macro *ud1-coord* (phase-1-coordinate-ud1 **phase-1-coordinate**))

(defun phase-1-solved-p (coord)
  (and (zerop (phase-1-coordinate-eo coord))
       (zerop (phase-1-coordinate-co coord))
       (zerop (phase-1-coordinate-ud1 coord))))

;;; G1 Coordinates

(defstruct phase-2-coordinate
  (ep 0 :type (integer 0 (#.+num-ep+)))
  (cp 0 :type (integer 0 (#.+num-cp+)))
  (ud2 0 :type (integer 0 (#.+num-ud2+))))

(declaim (type phase-2-coordinate **phase-2-coordinate**))
(global-vars:define-global-var* **phase-2-coordinate** (make-phase-2-coordinate))

(define-symbol-macro *ep-coord* (phase-2-coordinate-ep **phase-2-coordinate**))
(define-symbol-macro *cp-coord* (phase-2-coordinate-cp **phase-2-coordinate**))
(define-symbol-macro *ud2-coord* (phase-2-coordinate-ud2 **phase-2-coordinate**))

(defun phase-2-solved-p (coord)
  (and (zerop (phase-2-coordinate-ep coord))
       (zerop (phase-2-coordinate-cp coord))
       (zerop (phase-2-coordinate-ud2 coord))))

;;;;;;;;;;;;;;;;;;;;;;;;;;; Solution Moves ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconstant +max-phase-1-length+ 12)
(defconstant +max-phase-2-length+ 18)

(declaim (type (simple-array (or null move) (*)) **phase-1** **phase-2**))
(global-vars:define-global-var* **phase-1**
    (make-array +max-phase-1-length+ :element-type '(or null move)
                                     :initial-element nil))

(global-vars:define-global-var* **phase-2**
    (make-array +max-phase-2-length+ :element-type '(or null move)
                                     :initial-element nil))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Moves ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO: Change these values to use the symbolic constants instead of
;; numbers.

(alexandria:define-constant +c-cycles+
    #(#(0 1 2 3)     ; U
      #(4 5 6 7)     ; D
      #(0 3 5 4)     ; F
      #(1 7 6 2)     ; B
      #(0 4 7 1)     ; R
      #(2 6 5 3))    ; L
  :test 'equalp)

(alexandria:define-constant +e-cycles+
    #(#(0  1 2  3)   ; U
      #(4  7 6  5)   ; D
      #(0  9 4  8)   ; F
      #(2 10 6 11)   ; B
      #(1  8 5 10)   ; R
      #(3 11 7  9))  ; L
  :test 'equalp)

;;; <U, D> do not change the orientation of the corners.

(alexandria:define-constant +c-twists+
    #(#(0 0 0 0)     ; U
      #(0 0 0 0)     ; D
      #(2 1 2 1)     ; F
      #(1 2 1 2)     ; B
      #(1 2 1 2)     ; R
      #(1 2 1 2))    ; L
  :test 'equalp)

;;; <F, B> only change the orientation of the edges.

(alexandria:define-constant +e-twists+
    #(#(0 0 0 0)     ; U
      #(0 0 0 0)     ; D
      #(1 1 1 1)     ; F
      #(1 1 1 1)     ; B
      #(0 0 0 0)     ; R
      #(0 0 0 0))    ; L
  :test 'equalp)

(defun array-move (mv)
  (flet ((move-pieces (perm orie cycle twist m)
           (let* ((cycle-0 (aref cycle 0))
                  (ptmp (aref perm cycle-0))
                  (otmp (aref orie cycle-0)))
             (dotimes (i 3)
               (let ((p (aref cycle i)))
                 (setf (aref orie p)
                       (mod (+ (aref orie (aref cycle (1+ i)))
                               (aref twist (1+ i)))
                            m)

                       (aref perm p)
                       (aref perm (aref cycle (1+ i))))))
             (let ((last-cycle (aref cycle 3)))
               (setf (aref orie last-cycle)
                     (mod (+ otmp (aref twist 0)) m)
                     
                     (aref perm last-cycle)
                     ptmp)))))
    (multiple-value-bind (face multiplicity)
        (decompose-move mv)
      (dotimes (i multiplicity)
        ;; Move corners.
        (move-pieces **cp**
                     **co**
                     (aref +c-cycles+ face)
                     (aref +c-twists+ face)
                     3)
        
        ;; Move edges.
        (move-pieces **ep**
                     **eo**
                     (aref +e-cycles+ face)
                     (aref +e-twists+ face)
                     2)))))

(defun coord-move (mv)
  (multiple-value-bind (face multiplicity)
      (decompose-move mv)
    (dotimes (i multiplicity)
      (setf *eo-coord*  (aref *eo-trans*  *eo-coord*  face)
            *co-coord*  (aref *co-trans*  *co-coord*  face)
            *cp-coord*  (aref *cp-trans*  *cp-coord*  face)
            *ud1-coord* (aref *ud1-trans* *ud1-coord* face))
      (when (or (is-ud-move-p mv)          ; U or D?
                (and (zerop i)
                     (= 2 multiplicity)))  ; F2 B2 L2 R2?
        (setf *ep-coord*  (aref *ep-trans*  *ep-coord*  face)
              *ud2-coord* (aref *ud2-trans* *ud2-coord* face))))))

(defun do-move (mv)
  (array-move mv)
  (coord-move mv))


;;;;;;;;;;;;;;;;;;;;;; Coordinate Manipulations ;;;;;;;;;;;;;;;;;;;;;;

(defun set-eo-coord (coord)
  (loop :for i :from 10 :downto 0
        :for next-coord := coord :then (ash next-coord -1)
        :do (let ((ith-eo (logand next-coord 1)))
              (setf (aref **eo** i) ith-eo)
              ;; The orientation of the last edge is determined by
              ;; the orientation of the previous 11.
              (setf (aref **eo** 11) (logxor ith-eo (aref **eo** 11))))))

(defun get-eo-coord ()
  (ash (reduce (lambda (coord eo)
                 (ash (logior coord eo) 1))
               **eo**)
       -1))

(defun set-co-coord (coord)
  (loop :for i :from 6 :downto 0
        :for p := 729 :then (floor p 3) ; 9 + (factorial 6)
        :do (progn
              (setf (aref **co** i) (floor coord p))
              (decf coord (* (aref **co** i) p))
              ;; The orientation of the last corner is determined by
              ;; the orientation of the previous 7.
              (setf (aref **co** 7) (mod (+ (aref **co** 7)
                                          3
                                          (- (aref **co** i)))
                                       3)))))

(defun get-co-coord ()
  (loop :for i :below 7
        :for p := 1 :then (* 3 p)
        :sum (* (aref **co** i) p)))

(defun set-ud1-coord (coord)
  (map-into **ep** (constantly 0))

  (loop :with i := 11 :and j := 4
        :while (and (not (minusp i)) 
                    (not (zerop j)))
        :for binomial := (choose i (1- j))
        :if (>= coord binomial)
          :do (decf coord binomial)
        :else
          :do (progn (setf (aref **ep** i) 8)
                     (decf j))
        :do (decf i)))

(defun get-ud1-coord ()
  (let ((coord 0)
        (j 0))
    (dotimes (i 12 coord)
      (when (> (aref **ep** i) 7)
        (incf j))
      (when (and (not (zerop j))
                 (< (aref **ep** i) 8))
        (incf coord (choose i (1- j)))))))


(defun set-permutation-coordinate (perm offset len coord)
  (let ((used 0)
        (p (factorial (1- len))))
    (loop :for i :from (1- len) :downto 0 :do
      (let ((j (1- len)))
        (loop :until (zerop (logand used (ash 1 j)))
              :do (decf j))
        
        (loop :with k := 0
              :while (< k (floor coord p))
              :when (zerop (logand used (ash 1 j)))
                :do (incf k)
              :do (decf j))
        
        (loop :until (zerop (logand used (ash 1 j)))
              :do (decf j))
        
        (setf used (logior used (ash 1 j)))
        (setf (aref perm (+ i offset)) j)
        (setf coord (mod coord p))
        (when (plusp i)
          (setf p (floor p i)))))))

(defun get-permutation-coordinate (perm offset len)
  (loop 
    :for i :from 1
    :for p := 1 :then (* p i)
    :while (< i len)
    :sum (* p (loop :for j :below i
                    :count (> (aref perm (+ j offset))
                              (aref perm (+ i offset)))))))

(defun set-cp-coord (coord)
  (set-permutation-coordinate **cp** 0 +num-corners+ coord))

(defun get-cp-coord ()
  (get-permutation-coordinate **cp** 0 +num-corners+))

(defun set-ep-coord (coord)
  (set-permutation-coordinate **ep** 0 +num-corners+ coord))

(defun get-ep-coord ()
  (get-permutation-coordinate **ep** 0 +num-corners+))

(defun set-ud2-coord (coord)
  (set-permutation-coordinate **ep** 8 4 coord))

(defun get-ud2-coord ()
  (get-permutation-coordinate **ep** 8 4))

(defun clear-cube ()
  (setf *cp-coord* 0
        *co-coord* 0
        *ep-coord* 0
        *eo-coord* 0
        *ud1-coord* 0
        *ud2-coord* 0)
  
  (dotimes (i +num-corners+)
    (setf (aref **cp** i) i
          (aref **co** i) 0))
  
  (dotimes (i +num-edges+)
    (setf (aref **ep** i) i
          (aref **eo** i) 0)))


;;;;;;;;;;;;;;;;;;;;;;;;;;; Pruning Tables ;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun clear-pruning-tables ()
  (map-into *eo-prune*  (constantly +max-prune-distance+))
  (map-into *ep-prune*  (constantly +max-prune-distance+))
  (map-into *co-prune*  (constantly +max-prune-distance+))
  (map-into *cp-prune*  (constantly +max-prune-distance+))
  (map-into *ud1-prune* (constantly +max-prune-distance+))
  (map-into *ud2-prune* (constantly +max-prune-distance+)))

(defun initialize-pruning-tables ()
  (labels ((init-prune (group coord prune-table tran-table max-depth depth last)
             (declare (type (member :G0 :G1) group))
             (unless (or (= depth max-depth)
                         (>= depth (aref prune-table coord)))
               (setf (aref prune-table coord) depth)
               (let ((old coord))
                 (do-quarter-moves mv
                   (let ((mv-face (move-face mv))
                         (last-face (if (minusp last) 0 (move-face last))))
                     (unless (= mv-face last-face)
                       (dotimes (i 3)
                         (when (and (eql :G1 group)
                                    (not (is-ud-move-p mv))
                                    (not (zerop i)))
                           (return))
                         (setf coord (aref tran-table coord mv-face))
                         (init-prune group coord prune-table tran-table max-depth (1+ depth) mv))
                       (setf coord old))))))))
    
    (clear-pruning-tables)
    
    (init-prune ':G0 0 *eo-prune*  *eo-trans*  8 0 -1)
    (init-prune ':G0 0 *co-prune*  *co-trans*  7 0 -1)
    (init-prune ':G0 0 *ud1-prune* *ud1-trans* 6 0 -1)
    
    (init-prune ':G1 0 *cp-prune*  *cp-trans* 14 0 -1)
    (init-prune ':G1 0 *ep-prune*  *ep-trans*  9 0 -1)
    (init-prune ':G1 0 *ud2-prune* *ud2-trans* 5 0 -1)))


;;;;;;;;;;;;;;;;;;;;;;;;; Transition Tables ;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun initialize-transition-tables ()
  (flet ((init-trans (group trans-table set-coord get-coord)
           (declare (type (member :G0 :G1) group))
           (dotimes (i (array-dimension trans-table 0))
             (do-quarter-moves j
               (clear-cube)
               (funcall set-coord i)
               (array-move j)
               (when (and (eq :G1 group) (not (is-ud-move-p j)))
                 ;; In G1, do a double turn if the move is not U or D.
                 (array-move j))
               (setf (aref trans-table i (move-face j))
                     (funcall get-coord))))))
    (init-trans ':G0 *eo-trans*  #'set-eo-coord  #'get-eo-coord) 
    (init-trans ':G0 *co-trans*  #'set-co-coord  #'get-co-coord)
    (init-trans ':G0 *ud1-trans* #'set-ud1-coord #'get-ud1-coord)
    
    (init-trans ':G1 *cp-trans*  #'set-cp-coord  #'get-cp-coord)
    (init-trans ':G1 *ep-trans*  #'set-ep-coord  #'get-ep-coord)
    (init-trans ':G1 *ud2-trans* #'set-ud2-coord #'get-ud2-coord)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Phase 1 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; Phase-1 : <U,D,F,B,R,L> → <U,D,F2,B2,R2,L2>

(defun phase-1 (eo-coord co-coord ud1-coord depth)
  "Bring the cube from G0 to G1."
  (let ((eo-trans *eo-trans*)
        (co-trans *co-trans*)
        (ud1-trans *ud1-trans*)
        
        (eo-prune *eo-prune*)
        (co-prune *co-prune*)
        (ud1-prune *ud1-prune*))
    (labels ((dfs (eo-coord co-coord ud1-coord depth last-move)
               (declare (type fixnum eo-coord co-coord ud1-coord)
                        (type non-negative-fixnum depth)
                        (type (or null move) last-move))
               (cond
                 ;; Solved.
                 ((and (zerop eo-coord)
                       (zerop co-coord)
                       (zerop ud1-coord))
                  t)
                 
                 ;; The solution must be of at least the depths specified by each
                 ;; pruning table.
                 ((or (zerop depth)
                      (< depth (aref eo-prune eo-coord))
                      (< depth (aref co-prune co-coord))
                      (< depth (aref ud1-prune ud1-coord)))
                  nil)
                 
                 ;; Do a depth first search, trying all possible moves
                 (t
                  (do-quarter-moves mv
                    (let ((mv-face (move-face mv)))
                      (unless (and last-move
                                   (or (same-face-p mv last-move)   ; change
                                       (and (is-fru-move-p last-move)
                                            (parallel-moves-p mv last-move))))
                        (let ((eo-coord eo-coord)
                              (co-coord co-coord)
                              (ud1-coord ud1-coord))
                          (dotimes (mult 3)
                            (setf eo-coord  (aref eo-trans eo-coord mv-face)
                                  co-coord  (aref co-trans co-coord mv-face)
                                  ud1-coord (aref ud1-trans ud1-coord mv-face))
                            (when (dfs eo-coord co-coord ud1-coord (1- depth) (+ mv mult))
                              (setf (svref **phase-1** (1- depth)) (+ mv mult))
                              (return-from dfs t)))))))
                  ;; Still no solution found. Return NIL.
                  nil))))
      (dfs eo-coord co-coord ud1-coord depth nil))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Phase 2 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; Phase-2 : <U,D,F2,B2,R2,L2> → <I>

(defun phase-2 (ep-coord cp-coord ud2-coord depth)
  (let ((ep-trans *ep-trans*)
        (cp-trans *cp-trans*)
        (ud2-trans *ud2-trans*)
        
        (ep-prune *ep-prune*)
        (cp-prune *cp-prune*)
        (ud2-prune *ud2-prune*))
    (labels ((dfs (ep-coord cp-coord ud2-coord depth last-move)
               (declare (type fixnum ep-coord cp-coord ud2-coord)
                        (type non-negative-fixnum depth)
                        (type (or null move) last-move))
               (cond
                 ;; Solved.
                 ((and (zerop ep-coord)
                       (zerop cp-coord)
                       (zerop ud2-coord))
                  t)
                 
                 ;; The solution must be of at least the depths specified by each
                 ;; pruning table.
                 ((or (zerop depth)
                      (< depth (aref ep-prune ep-coord))
                      (< depth (aref cp-prune cp-coord))
                      (< depth (aref ud2-prune ud2-coord)))
                  nil)
                 
                 (t
                  (do-quarter-moves mv
                    (let ((mv-face (move-face mv)))
                      (unless (and last-move
                                   (or (same-face-p mv last-move)
                                       (and (is-fru-move-p last-move)
                                            (parallel-moves-p mv last-move))))
                        (let ((ep-coord ep-coord)
                              (cp-coord cp-coord)
                              (ud2-coord ud2-coord))
                          (loop :with ud-move? := (is-ud-move-p mv)
                                :for mult ; We only want double turns for F, R, and U.
                                :from (if ud-move? 0 1) 
                                  :below (if ud-move? 3 2)
                                :do (progn
                                      (setf ep-coord  (aref ep-trans ep-coord mv-face)
                                            cp-coord  (aref cp-trans cp-coord mv-face)
                                            ud2-coord (aref ud2-trans ud2-coord mv-face))
                                      (when (dfs ep-coord cp-coord ud2-coord (1- depth) (+ mv mult))
                                        (setf (svref **phase-2** (1- depth)) (+ mv mult))
                                        (return-from dfs t))))))))
                  ;; Still no solution found. Return NIL.
                  nil))))
      (dfs ep-coord cp-coord ud2-coord depth nil))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Setup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun set-cube (configuration)
  "Set the cube from the Reid configuration string CONFIGURATION."
  (let ((edge-names #("UFU" "URU" "UBU" "ULU"
                      "DFD" "DRD" "DBD" "DLD"
                      "FRF" "FLF" "BRB" "BLB"))
        (corner-names #("UFRUF" "URBUR" "UBLUB" "ULFUL"
                        "DRFDR" "DFLDF" "DLBDL" "DBRDB")))
    (flet ((locate (substring strings)
             (loop :for pos :from 0
                   :for string :across strings
                   :do (let ((search (search substring string :test #'char=)))
                         (when search
                           (return-from locate (values pos search)))))
             ;; If we get to this point, it's an error.
             (error "Invalid substring ~S" substring)))
      (declare (inline locate))
      (let* ((split (split-sequence:split-sequence #\Space configuration))
             (edges (subseq split 0 12))
             (corners (subseq split 12)))
        
        ;; Edges
        (loop :for i :below +num-edges+
              :for edge :in edges
              :do (multiple-value-bind (piece orientation)
                      (locate edge edge-names)
                    (setf (aref **ep** i) piece
                          (aref **eo** i) orientation)))
        
        ;; Corners
        (loop :for i :below +num-corners+
              :for corner :in corners
              :do (multiple-value-bind (piece orientation)
                      (locate corner corner-names)
                    (setf (aref **cp** i) piece
                          (aref **co** i) orientation)))
        
        (setf *co-coord* (get-co-coord)
              *eo-coord* (get-eo-coord)
              *ud1-coord* (get-ud1-coord))))))

(defun initialize-tables ()
  "Initialize the transition tables and pruning tables."
  (format t "Initializing tables...")
  (let ((start (get-time)))
    (initialize-transition-tables)
    (initialize-pruning-tables)
    (setf *tables-initialized* t)
    (format t " [~Dms]~%" (get-time-since start))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Solving ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun extract-solution (phase-1-moves phase-2-moves)
  (let ((solution nil))
    (loop :for i :from (1- +max-phase-1-length+) :downto 0
          :for mv := (aref phase-1-moves i)
          :when mv
            :do (push mv solution))
    (loop :for i :from (1- +max-phase-2-length+) :downto 0
          :for mv := (aref phase-2-moves i)
          :when mv
            :do (push mv solution))
    (simplify-moves (nreverse solution))))

(alexandria:define-constant +solved-configuration+
  "UF UR UB UL DF DR DB DL FR FL BR BL UFR URB UBL ULF DRF DFL DLB DBR"
  :test #'string=)

(defun solve (configuration &aux (start (get-time)))
  "Solve the configuration CONFIGURATION."
  ;; Reset the moves.
  (map-into **phase-1** (constantly nil))
  (map-into **phase-2** (constantly nil))
  
  ;; Initialize tables if needed.
  (unless *tables-initialized*
    (initialize-tables))
  
  ;; Set up the cube.
  (set-cube configuration)
  
  ;; IDDFS of Phase 1
  (write-string ".")
  (loop :for i :from 0
        :until (phase-1 *eo-coord* *co-coord* *ud1-coord* i)
        :do (progn
              (write-string ".") ; depth = 1+i
              (finish-output)))
  
  ;; Apply moves to cube from Phase 1
  (loop :for i :from (1- +max-phase-1-length+) :downto 0
        :for mv := (aref **phase-1** i)
        :when mv
          :do (do-move mv))
  
  (setf *ep-coord* (get-ep-coord)
        *cp-coord* (get-cp-coord)
        *ud2-coord* (get-ud2-coord))
  
  ;; IDDFS: Phase 2
  (write-string "/.")
  (loop :for i :from 0
        :until (phase-2 *ep-coord* *cp-coord* *ud2-coord* i)
        :do (progn
              (write-string ".") ; depth = 1+i
              (finish-output)))
  
  ;; Return solution.
  (let ((solution (extract-solution **phase-1** **phase-2**)))
    (format t " [~Dms]~%" (get-time-since start))

    (if (null solution)
        "I"
        (with-output-to-string (*standard-output*)
          (mapc (lambda (mv)
                  (write-string (move-string mv)))
                solution)))))

(defun solve-configurations (in-file out-file &key max)
  (unless *tables-initialized*
    (initialize-tables))
  
  (with-open-file (in in-file :direction :input)
    (with-open-file (out out-file :direction :output
                                  :if-exists :supersede
                                  :if-does-not-exist :create)
      (loop :for configuration := (read-line in nil nil nil)
              :then (read-line in nil nil nil)
            :for i :from 1
            :while (and configuration (or (null max) (<= i max)))
            :if (= 67 (length configuration))
              :do (handler-case (prog1
                                  (write-string (solve configuration) out)
                                  (terpri out)
                                  (finish-output out))
                    (error (c) 
                      (declare (ignore c))
                      (format t "~&Failed: ~S~%" configuration)))
            :else
              :do (format t "Skipping invalid configuration: ~S~%" configuration)))))
