;;;; group.lisp
;;;;
;;;; Copyright (c) 2013 Robert Smith

(in-package #:qsolve)

;;;; TODO: Generate cubie cube structures directly.


;;;; Sticker Cube Layout

;;                +--------------+
;;                |              |
;;                |  1    2    3 |
;;                |              |
;;                |  4   up    5 |
;;                |              |
;;                |  6    7    8 |
;;                |              |
;; +--------------+--------------+--------------+--------------+
;; |              |              |              |              |
;; |  9   10   11 | 17   18   19 | 25   26   27 | 33   34   35 |
;; |              |              |              |              |
;; | 12  left  13 | 20 front  21 | 28 right  29 | 36  back  37 |
;; |              |              |              |              |
;; | 14   15   16 | 22   23   24 | 30   31   32 | 38   39   40 |
;; |              |              |              |              |
;; +--------------+--------------+--------------+--------------+
;;                |              |
;;                | 41   42   43 |
;;                |              |
;;                | 44  down  45 |
;;                |              |
;;                | 46   47   48 |
;;                |              |
;;                +--------------+

(deftype sticker ()
  `(integer 1 48))

(defun sticker-face (sticker)
  (ecase sticker
    (( 1  2  3  4  5  6  7  8) up)
    (( 9 10 11 12 13 14 15 16) left)
    ((17 18 19 20 21 22 23 24) front)
    ((25 26 27 28 29 30 31 32) right)
    ((33 34 35 36 37 38 39 40) back)
    ((41 42 43 44 45 46 47 48) down)))

(defun corner-stickers (corner)
  (cdr (assoc corner
              (constant-load-time-value
               (list (list UFR 8 19 25)
                     (list URB 3 27 33)
                     (list UBL 1 35 9)
                     (list ULF 6 11 17)
                     
                     (list DRF 43 30 24)
                     (list DFL 41 22 16)
                     (list DLB 46 14 40)
                     (list DBR 48 38 32))))))

(defun edge-stickers (edge)
  (cdr (assoc edge
              (constant-load-time-value
               (list (list UF 7 18)
                     (list UR 5 26)
                     (list UB 2 34)
                     (list UL 4 10)
               
                     (list DF 42 23)
                     (list DR 45 31)
                     (list DB 47 39)
                     (list DL 44 15)
               
                     (list FR 21 28)
                     (list FL 20 13)
                     (list BR 36 29)
                     (list BL 37 12))))))

(defun sticker-cube-to-cubie-cube (sticker-seq)
  (with-output-to-string (s)
    (labels ((sticker-ref (n)
               (elt sticker-seq (1- n)))
             
             (stickers-to-cubie (stickers)
               (apply 'concatenate
                      'string
                      (mapcar #'face-string
                              (mapcar #'sticker-face
                                      (mapcar #'sticker-ref stickers))))))
      (format s "~{~A~^ ~}"
              (mapcar #'stickers-to-cubie
                      (append
                       (mapcar #'edge-stickers edge-list)
                       (mapcar #'corner-stickers corner-list)))))))

(defvar *cube-group*
  (perm:group-from
   '((3 5 8 2 7 1 4 6 33 34 35 12 13 14 15 16 9 10 11 20 21 22 23 24 17
      18 19 28 29 30 31 32 25 26 27 36 37 38 39 40 41 42 43 44 45 46 47 48)
     (17 2 3 20 5 22 7 8 11 13 16 10 15 9 12 14 41 18 19 44 21 46 23 24
      25 26 27 28 29 30 31 32 33 34 6 36 4 38 39 1 40 42 43 37 45 35 47 48)
     (1 2 3 4 5 25 28 30 9 10 8 12 7 14 15 6 19 21 24 18 23 17 20 22 43
      26 27 42 29 41 31 32 33 34 35 36 37 38 39 40 11 13 16 44 45 46 47 48)
     (1 2 38 4 36 6 7 33 9 10 11 12 13 14 15 16 17 18 3 20 5 22 23 8 27
      29 32 26 31 25 28 30 48 34 35 45 37 43 39 40 41 42 19 44 21 46 47 24)
     (14 12 9 4 5 6 7 8 46 10 11 47 13 48 15 16 17 18 19 20 21 22 23 24
      25 26 1 28 2 30 31 3 35 37 40 34 39 33 36 38 41 42 43 44 45 32 29 27)
     (1 2 3 4 5 6 7 8 9 10 11 12 13 22 23 24 17 18 19 20 21 30 31 32 25
      26 27 28 29 38 39 40 33 34 35 36 37 14 15 16 43 45 48 42 47 41 44 46)))
  "Definition of the 3x3 Rubik's cube group.")


(defun perm-to-cubie-cube (perm)
  (sticker-cube-to-cubie-cube (perm:perm-to-list perm)))

(defun random-configuration ()
  "Truly random cube generator."
  (perm-to-cubie-cube (perm:random-group-element *cube-group*)))

