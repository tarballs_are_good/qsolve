;;;; tests.lisp
;;;; Copyright (c) 2013 Robert Smith

(in-package #:qsolve)

(defun make-tests (size filename)
  (with-open-file (s filename :direction :output
                              :if-does-not-exist :create
                              :if-exists :supersede)
    (loop :for i :below size
          :for r := (random-configuration)
          :collect (list i r (solve r)) :into solutions
          :finally (pprint solutions s))))

;;; TODO: Make a better test for solution equality.

(defun execute-tests (sexp-file)
  (with-open-file (s sexp-file :direction :input
                               :if-does-not-exist :error)
    (with-standard-io-syntax
      (let* ((*read-eval* nil)
             (tests (read s)))
        (loop :for (test-num config solution) :in tests
              :for config-solution := (solve config)
              :do (if (string-equal solution config-solution)
                      (write-string ".")
                      (format t "~&Test #~D failed! Expected ~S but received ~S for configuration ~S.~%" test-num solution config-solution config))
              :finally (terpri))))))
