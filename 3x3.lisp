;;;; 3x3.lisp
;;;; Copyright (c) 2011-2012 Robert Smith

;;;; 3x3 cube stuff

(in-package #:qsolve)

(defconstant +3x3-sticker-count+ 48)

(defun make-3x3-cube ()
  (perm:perm-identity +3x3-sticker-count+))

(defvar *3x3-moves* (make-hash-table))

(defun register-move (name perm)
  (setf (gethash name *3x3-moves*) perm))

;;;                     +--------------+
;;;                     |              |
;;;                     |  1    2    3 |
;;;                     |              |
;;;                     |  4   up    5 |
;;;                     |              |
;;;                     |  6    7    8 |
;;;                     |              |
;;;      +--------------+--------------+--------------+--------------+
;;;      |              |              |              |              |
;;;      |  9   10   11 | 17   18   19 | 25   26   27 | 33   34   35 |
;;;      |              |              |              |              |
;;;      | 12  left  13 | 20 front  21 | 28 right  29 | 36  back  37 |
;;;      |              |              |              |              |
;;;      | 14   15   16 | 22   23   24 | 30   31   32 | 38   39   40 |
;;;      |              |              |              |              |
;;;      +--------------+--------------+--------------+--------------+
;;;                     |              |
;;;                     | 41   42   43 |
;;;                     |              |
;;;                     | 44  down  45 |
;;;                     |              |
;;;                     | 46   47   48 |
;;;                     |              |
;;;                     +--------------+

(defun initialize-3x3-moves ()
  (flet ((cycles->perm (cs)
           (perm:from-cycles
            (mapcar
             (lambda (cycle) (apply #'perm:make-cycle cycle))
             cs)
            +3x3-sticker-count+)))
    (register-move 'F (cycles->perm '((17 19 24 22) (18 21 23 20)
                                      (6 25 43 16) (7 28 42 13)
                                      (8 30 41 11))))
    
    (register-move 'R (cycles->perm '((25 27 32 30) (26 29 31 28)
                                      (3 38 43 19) (5 36 45 21)
                                      (8 33 48 24))))
    
    (register-move 'U (cycles->perm '((1 3 8 6) (2 5 7 4)
                                      (9 33 25 17) (10 34 26 18)
                                      (11 35 27 19))))
    
    (register-move 'B (cycles->perm '((33 35 40 38) (34 37 39 36)
                                      (3 9 46 32) (2 12 47 29)
                                      (1 14 48 27))))
    
    (register-move 'L (cycles->perm '((9 11 16 14) (10 13 15 12)
                                      (1 17 41 40) (4 20 44 37)
                                      (6 22 46 35))))
    
    (register-move 'D (cycles->perm '((41 43 48 46) (42 45 47 44)
                                      (14 22 30 38) (15 23 31 39)
                                      (16 24 32 40))))
    
    (values)))

(defvar +3x3-generator+ '(F R U B L D))

(defun interpret-move (move)
  (etypecase move
    (symbol (gethash move *3x3-moves*))
    (list (let ((base (interpret-move (first move)))
                (amount (second move)))
            (perm:perm-expt base amount)))))

(defun interpret-moves (moves)
  (reduce (lambda (acc move)
            (perm:perm-compose (interpret-move move)
                               acc))
          moves
          :initial-value (make-3x3-cube)))





